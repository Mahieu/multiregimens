import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, BaseEntity, ManyToOne } from "typeorm/browser";
import { Food } from "./food.entity";

@Entity({name:"Meals"})
export class Meal extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    quantity: number;
    @Column({type: 'datetime'})
    eatDate : Date;
    @ManyToOne(type => Food,{ eager: true })
    @JoinColumn({ name: "food_id" })
    food: Food;
  }
