import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm/browser';
@Entity({
    name: "Foods"})
export class Food extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name:string;
    @Column()
    displayName: string;
    @Column()
    portionDisplayName: string;
    @Column()
    servingDesc: string;
    @Column()
    category : string;
    @Column()
    points : number;
    @Column()
    pointsPrecise : number;
    @Column()
    weight : string;
    @Column()
    weightType : string;
    @Column({nullable : true})
    brand : string;
}