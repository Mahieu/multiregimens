import { platformNativeScriptDynamic } from 'nativescript-angular/platform';
import { AppModule } from './app.module';
import { enableProdMode } from '@angular/core';
import {createConnection, getManager} from "typeorm/browser";
import { Meal } from './models/meal.entity';
import { Food } from './models/food.entity';
let driver = require('nativescript-sqlite');
(async () => {
    try {
        const connection = await createConnection({
            database: 'app.db',
            type: 'nativescript',
            driver :driver,
            entities: [
                Food,
                Meal
            ],
            logging: true,
            synchronize : false
        })
        console.log("Connection Created")
        console.log("Synchronized")


    } catch (err) {
        console.error(err)
    }
})();
enableProdMode();
platformNativeScriptDynamic().bootstrapModule(AppModule);