import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Food } from './models/food.entity';

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  private foodSource = new BehaviorSubject(new Food());
  selectedFood = this.foodSource.asObservable();
  constructor() { }
  selectFood(food : Food){
    this.foodSource.next(food);
  }
}
