import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from "nativescript-angular/router";


import { HomeComponent } from './home/home.component';
import { FoodComponent } from './food/food.component';
import { SettingsComponent } from './settings/settings.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FoodDetailComponent } from './food-detail/food-detail.component';

const routes: Routes = [
  { path: "", redirectTo: '/(home:home//settings:settings//dashboard:dashboard)', pathMatch: 'full'},
  { path: "home", component: HomeComponent, outlet: "home"},
  { path: "food", component: FoodComponent, outlet: "home"},
  { path: "food-detail", component: FoodDetailComponent, outlet: "home"},
  { path: "settings", component: SettingsComponent, outlet: "settings"},
  { path: "dashboard", component: DashboardComponent, outlet: "dashboard"},
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
