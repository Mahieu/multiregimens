import { Component } from '@angular/core';
import {Page} from "ui/page";
var Sqlite = require("nativescript-sqlite");
@Component({
  selector: 'app-root',
  templateUrl: "app.component.html"})
export class AppComponent {
  constructor(private page: Page) {
    new Sqlite("app.db", function(err,db) {
      db.version(function(err,ver){
        if(ver ===0){
          Sqlite.copyDatabase('app.db');
          db.version(1);
        }
      });
    });
  }

  ngOnInit(): void {
      this.page.actionBarHidden = true;
  }
}

