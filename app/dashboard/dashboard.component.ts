import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core/ui/page/page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  moduleId: module.id,
})
export class DashboardComponent implements OnInit {

  constructor(private page: Page) {
  }

  ngOnInit(): void {
      this.page.actionBarHidden = true;
  }

}
