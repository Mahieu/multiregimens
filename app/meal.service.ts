import { Injectable } from '@angular/core';
import { Meal } from './models/meal.entity';
import { Between } from 'typeorm/browser';

@Injectable({
  providedIn: 'root'
})
export class MealService {

  constructor() { }
  async findBetweenDates(start : Date,end:Date) : Promise<Array<Meal>>{
    await Meal.find({
      where : {
        eatDate : Between(start,end)
      }
    }).then((meals) => {return meals;}).catch(err => console.log(err));
    return null;
  }
  findAll(){
     Meal.find().then(meals => {return meals;}).catch(err => console.log(err));
  }
}
