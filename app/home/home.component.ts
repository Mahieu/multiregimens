import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core/ui/page/page';
import { RouterExtensions } from 'nativescript-angular/router';
import * as appSettings from "application-settings";
import { Meal } from '../models/meal.entity';
import { ObservableArray } from '@nativescript/core/data/observable-array/observable-array';
import { Food } from '../models/food.entity';
import { MealService } from '../meal.service';
import { Between } from 'typeorm/browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public pointsLeft : number = 42;
  public pointsLeftWeekly: number = 42;
  public countMeals: number = 0;
  public meals : ObservableArray<Meal> = new ObservableArray<Meal>();
  constructor(private page: Page,private routerExtensions: RouterExtensions, private mealService:MealService) {
  }

  ngOnInit(): void {
      this.page.actionBarHidden = true;
      this.loadMeals();
      this.page.on('navigatedTo', (data) => {               
      this.loadMeals();
      });
      this.pointsLeft = appSettings.getNumber("pointsDaily");
      this.pointsLeftWeekly = appSettings.getNumber("pointsWeekly");
      /**if(appSettings.getNumber("currentPointsWeek") == null){
        appSettings.setNumber("currentPointsWeek",appSettings.getNumber("pointsWeekly"));
        appSettings.setString("currentWeekDate",this.findStartWeek(new Date()).toDateString());
      }
      else if(appSettings.getString("currentWeekDate") != this.findStartWeek(new Date()).toDateString())
      {
        appSettings.setString("currentWeekDate",this.findStartWeek(new Date()).toDateString());
        appSettings.setNumber("currentPointsWeek",appSettings.getNumber("pointsWeekly"));
      }
      else
      {
        this.pointsLeftWeekly = appSettings.getNumber("currentPointsWeek");
      }
      */
  }
  callFood() : void {
    this.routerExtensions.navigateByUrl("food");
  }
  loadMeals(): void {
    Meal.find({
      eatDate : Between(this.findStartToday().toISOString(),new Date().toISOString())
    }).then((meals) => {this.meals = new ObservableArray<Meal>(meals);this.calculatePointsToday();}).catch(err => {console.log(err);});
  }
  findStartToday(){
    let start : Date = new Date();
    start.setHours(0,0,0,1);
    return start;
  }
  findStartWeek(d) : Date{
    d = new Date(d);
    var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }
  calculatePointsToday(){
    let sumOfPoints = 0;
    this.meals.forEach((meal) => {
      sumOfPoints+=(meal.quantity * meal.food.points);
    });
    this.pointsLeft = appSettings.getNumber("pointsDaily") - sumOfPoints;
    if(this.pointsLeft <0)
    {
      this.pointsLeft =0;
    }
  }
}
