import {
  Component,
  OnInit
} from '@angular/core';
import {
  Page
} from '@nativescript/core/ui/page/page';
import {
  Food
} from '../models/food.entity';
import { Like } from 'typeorm/browser';
import { ObservableArray } from '@nativescript/core/data/observable-array/observable-array';
import { RouterExtensions } from 'nativescript-angular/router';
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { FoodService } from '../food.service';
import { ActivatedRoute } from '@angular/router';
const SQLite = require("nativescript-sqlite");
@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css'],
  moduleId: module.id,
})
export class FoodComponent implements OnInit {
  public searchPhrase: String;
  public foods: ObservableArray<Food>  = new ObservableArray<Food>();
  constructor(private page: Page,private routerExtensions: RouterExtensions,private foodService : FoodService,private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.page.actionBarHidden = true;
  }
  onSubmit(event): void {
    this.getFood();
  }
  onItemTap(event : ItemEventData) : void {
    this.foodService.selectFood(this.foods.getItem(event.index));
    this.routerExtensions.navigate(["../food-detail"], {relativeTo: this.activeRoute});
  }
  getFood(){
    Food.find({
      where: [
        {category: Like(`%${this.searchPhrase}%`)},
        {name: Like(`%${this.searchPhrase}%`)},
        {displayName: Like(`%${this.searchPhrase}%`)}
      ]
    }).then((tmpFoods) => {
      this.foods = new ObservableArray<Food>(tmpFoods);
    }).catch(console.error);
  }
}