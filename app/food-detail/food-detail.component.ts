import { Component, OnInit } from '@angular/core';
import { FoodService } from '../food.service';
import { Food } from '../models/food.entity';
import { Meal } from '../models/meal.entity';
import { Page } from 'tns-core-modules/ui/page/page';
import { RouterExtensions } from 'nativescript-angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-food-detail',
  templateUrl: './food-detail.component.html',
  styleUrls: ['./food-detail.component.css']
})
export class FoodDetailComponent implements OnInit {
  selectedFood:Food;
  quantity : number = 1;
  constructor(private foodService : FoodService, private page : Page, private routerExtensions : RouterExtensions,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.page.actionBarHidden = true;
    this.foodService.selectedFood.subscribe(food => this.selectedFood=food);
  }
  showFood(){
    alert(this.selectedFood.name);
  }
  addMeal(){
    let tmpMeal = new Meal();
    tmpMeal.eatDate = new Date();
    tmpMeal.quantity = this.quantity;
    tmpMeal.food=this.selectedFood;
    Meal.save(tmpMeal);
    this.routerExtensions.navigate(["../home"], {relativeTo: this.activeRoute});
  }
}
