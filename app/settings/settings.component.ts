import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core/ui/page/page';
import * as appSettings from "application-settings";
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  moduleId: module.id,
})
export class SettingsComponent implements OnInit {
  public pointsDaily : string;
  public pointsWeekly : string;
  constructor(private page: Page) {
  }

  ngOnInit(): void {
      this.page.actionBarHidden = true;
  }
  saveSettings() : void {
    appSettings.setNumber("pointsDaily",parseInt(this.pointsDaily));
    appSettings.setNumber("pointsWeekly",parseInt(this.pointsWeekly));
  }
}
